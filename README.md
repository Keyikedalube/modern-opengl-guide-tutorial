Learning OpenGL for fun :)

Online book - https://raw.githubusercontent.com/Overv/Open.GL/master/ebook/Modern%20OpenGL%20Guide.pdf

Implemented programs using **`C` and `SDL`**, the book uses **`C++` and `SFML`**
