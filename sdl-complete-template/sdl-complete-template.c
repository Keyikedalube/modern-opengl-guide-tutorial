#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

int main(int argc, char *argv[])
{
	SDL_Init(SDL_INIT_EVERYTHING);
	
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	
	SDL_Window *window = SDL_CreateWindow("OpenGL",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			400, 320, SDL_WINDOW_OPENGL);
	
	SDL_GLContext context = SDL_GL_CreateContext(window);
	
	glewExperimental = GL_TRUE;
	glewInit();
	
	GLuint vbo;
	glGenBuffers(1, &vbo);
	
	printf("%u\n", vbo);
	
	SDL_Event window_event;
	while (1) {
		if (SDL_PollEvent(&window_event) && window_event.type == SDL_QUIT)
			break;
		SDL_GL_SwapWindow(window);
	}
	
	glDeleteBuffers(1, &vbo);
	
	SDL_GL_DeleteContext(context);
	SDL_Quit();
	return(0);
}
