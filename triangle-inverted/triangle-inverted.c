/*
 * Ex: Alter the vertex shader so that the triangle is upside down
 */

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

int main(int argc, char *argv[])
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	SDL_Window *window = SDL_CreateWindow("Triangle inverted",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			400, 320, SDL_WINDOW_OPENGL);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	glewExperimental = GL_TRUE;
	glewInit();

	// Our vertex array objects
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Our vertex buffer
	GLuint vbo;
	glGenBuffers(1, &vbo);

	float vertices[] = {
		 0.0f,  0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Our vertex shader
	const char *vertex_source =
			"#version 150 core\n"
			"in vec2 position;\n"\
			"in vec3 color;\n"
			"out vec3 Color;\n"
			"void main()\n"
			"{\n"
			"	Color = color;\n"
			"	gl_Position = vec4(position.x, -position.y, 0.0, 1.0);\n"
			"}\n";

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source, NULL);

	glCompileShader(vertex_shader);

	// vertex shader compilation error log
	GLint status;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE) {
		char buffer[512];
		glGetShaderInfoLog(vertex_shader, 512, NULL, buffer);
		printf("%s\n", buffer);
	}

	// Our fragment shader
	const char *fragment_source =
			"#version 150 core\n"
			"in vec3 Color;\n"
			"out vec4 out_color;\n"
			"void main()\n"
			"{\n"
			"	out_color = vec4(Color, 1.0);\n"
			"}\n";
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source, NULL);

	glCompileShader(fragment_shader);

	// Connect our vertex and fragment shader
	GLuint shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);

	glBindFragDataLocation(shader_program, 0, "out_color");

	glLinkProgram(shader_program);
	glUseProgram(shader_program);

	// Uniform color red - uniform allows changing shader behaviors at runtime
	GLint uniform_color = glGetUniformLocation(shader_program, "triangle_color");
	glUniform3f(uniform_color, 1.0f, 0.0f, 0.0f);

	// Link our vertex data and attributes
	GLint pos_attrib = glGetAttribLocation(shader_program, "position");
	glEnableVertexAttribArray(pos_attrib);
	glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), 0);

	GLint col_attrib = glGetAttribLocation(shader_program, "color");
	glEnableVertexAttribArray(col_attrib);
	glVertexAttribPointer(col_attrib, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(2*sizeof(float)));


	SDL_Event window_event;
	while (1) {
		if (SDL_PollEvent(&window_event))
			if (window_event.type == SDL_QUIT) break;

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// triangle drawing
		glDrawArrays(GL_TRIANGLES, 0, 3);

		SDL_GL_SwapWindow(window);
	}

	glDeleteProgram(shader_program);

	glDeleteShader(fragment_shader);
	glDeleteShader(vertex_shader);

	glDeleteBuffers     (1, &vbo);
	glDeleteVertexArrays(1, &vao);

	SDL_GL_DeleteContext(context);
	SDL_Quit();
	return(0);
}
